//
//  Item.swift
//  Todoey
//
//  Created by Pawel Czarnecki on 27/10/2018.
//  Copyright © 2018 Pawel Czarnecki. All rights reserved.
//

import Foundation
import RealmSwift

class Item: Object{
    @objc dynamic var title : String = ""
    @objc dynamic var done : Bool = false
    @objc dynamic var dataCreated : Date?
    let parentCategory = LinkingObjects(fromType: Category.self, property: "items")
}
