//
//  Data.swift
//  Todoey
//
//  Created by Pawel Czarnecki on 27/10/2018.
//  Copyright © 2018 Pawel Czarnecki. All rights reserved.
//

import Foundation
import RealmSwift

class Data: Object {
    @objc dynamic var name : String = ""
    @objc dynamic var age : Int = 0
}
