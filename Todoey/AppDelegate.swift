//
//  AppDelegate.swift
//  Todoey
//
//  Created by Pawel Czarnecki on 21/10/2018.
//  Copyright © 2018 Pawel Czarnecki. All rights reserved.
//

import UIKit
import CoreData
import RealmSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        do{
            _ =  try Realm()
        } catch {
            print(error)
        }
        
        return true
    }

    




}

