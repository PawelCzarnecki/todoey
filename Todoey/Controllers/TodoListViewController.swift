//
//  ViewController.swift
//  Todoey
//
//  Created by Pawel Czarnecki on 21/10/2018.
//  Copyright © 2018 Pawel Czarnecki. All rights reserved.
//

import UIKit
import RealmSwift
import ChameleonFramework

class TodoListViewController: SwipeTableViewController {


    var toDoItems : Results<Item>?
    let realm = try! Realm()
    
    @IBOutlet weak var searchBar: UISearchBar!
    
    var selectedCategory : Category? {
        didSet{
           loadItems()
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.separatorStyle = .none
        tableView.rowHeight = 80
    }
    
    override func viewWillAppear(_ animated: Bool) {

            title = selectedCategory?.name
        
            guard let colorHex = selectedCategory?.cellColor else {fatalError("Color hex does not exists")}
            guard let navBarColor = UIColor(hexString: colorHex) else {fatalError("Nav bar color does not exists")}

            updateNavBar(withHextCode: navBarColor)

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        guard let originalColor = UIColor(hexString: UIColor.white.hexValue()) else {fatalError()}
        updateNavBar(withHextCode: originalColor)

    }
    
    func updateNavBar(withHextCode colorHexCode: UIColor){
        guard let navBar = navigationController?.navigationBar else {fatalError("Navi controller does not exists")}
        
        navBar.tintColor = ContrastColorOf(colorHexCode, returnFlat: true)
        navBar.barTintColor = colorHexCode
        navBar.largeTitleTextAttributes = [NSAttributedString.Key.foregroundColor : ContrastColorOf(colorHexCode, returnFlat: true)]
        navBar.tintColor = FlatWhite()
        searchBar.barTintColor = colorHexCode
        
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return toDoItems?.count ?? 1
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell = super.tableView(tableView, cellForRowAt: indexPath)
        
        if let item = toDoItems?[indexPath.row]{
        
            cell.textLabel?.text = item.title
            
            if let color = UIColor(hexString: selectedCategory!.cellColor)?.darken(byPercentage: CGFloat(indexPath.row)/CGFloat(toDoItems!.count)){
                cell.backgroundColor = color
                cell.textLabel?.textColor = ContrastColorOf(color, returnFlat: true)
            }
            
            cell.accessoryType = item.done ? .checkmark : .none
        }else {
            cell.textLabel?.text = "No items added"
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        if let item = toDoItems?[indexPath.row]{
            do{
                try realm.write {
                   item.done = !item.done
                }
            }catch{
                print(error)
            }
        }

        tableView.reloadData()
        tableView.deselectRow(at: indexPath, animated: true)
    }

    @IBAction func addButtonPressed(_ sender: UIBarButtonItem) {
        
        var textField = UITextField()
        
        let alertController = UIAlertController(title: "Add new todoey item", message: "", preferredStyle: .alert)
        let action = UIAlertAction(title: "Add Item", style: .default) { (action) in
            do{
                if let currentCategory = self.selectedCategory{
                    try self.realm.write {
                        let newItem = Item()
                        newItem.title = textField.text!
                        newItem.dataCreated = Date()
                        currentCategory.items.append(newItem)
                    }
                }
            } catch{
                print(error)
            }
            self.tableView.reloadData()


        }
        alertController.addTextField { (alertTextField) in
            alertTextField.placeholder = "Create new item"
            textField = alertTextField
        }
        
        
        alertController.addAction(action)
        
        present(alertController, animated: true, completion: nil)
        
    }
    
    func loadItems(){
        
      toDoItems = selectedCategory?.items.sorted(byKeyPath: "title", ascending: true)

        tableView.reloadData()
    }
    override func updateModel(at indexPath: IndexPath) {
        super.updateModel(at: indexPath)
        
        if let taskForDeletion = self.toDoItems?[indexPath.row]{
            do{
                try self.realm.write {
                    self.realm.delete(taskForDeletion)
                }
            }catch{
                print(error)
            }
        }
    }

}

    



//MARK: - Search bar methods
extension TodoListViewController: UISearchBarDelegate{

    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        toDoItems = toDoItems?.filter("title CONTAINS[cd] %@", searchBar.text!).sorted(byKeyPath: "dataCreated", ascending: true)
        tableView.reloadData()

    }

    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchBar.text?.count == 0 {
            loadItems()

            DispatchQueue.main.async {
                searchBar.resignFirstResponder()
            }

        }
    }


}

